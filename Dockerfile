FROM golang:1.12.5 as builder
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main main.go

FROM scratch
ADD http/ /http
COPY --from=builder /go/main /
CMD ["/main"]
