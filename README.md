# Simple Go Server

## What is this?

A simple webserver build in Go for use on Google Cloud Run. This will serve static content out of the `/http` folder and will do so on port 80 by default. It also accepts a `PORT` environment variable, and will listen on that port if given. Thus, a perfect set up for Cloud Run which only will let you use the `PORT` env var.
